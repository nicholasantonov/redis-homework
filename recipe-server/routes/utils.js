const recipeData = require("../../recipe-data");
const uuid = require("node-uuid");
const jwt = require('jsonwebtoken');
const redis = require('promise-redis')().createClient();

function authToken (req, res, next) {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, 'superSecret', function(err, decoded) {
            if (err) {
                res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                redis.hget('token', token)
                    .then(validity => {
                        if (validity === 'true') {
                            req.user = decoded;
                            req.token = token;
                            next();
                        }
                    });
            }
        });

    } else {
        res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
}

function createRoute (action, response) {
    return function (req, res) {
        const redisConnection = req
            .app
            .get("redis");

        const messageId = uuid.v4();
        let killswitchTimeoutId = undefined;

        redisConnection.on(`${response}:${messageId}`, (result, channel) => {
            res.json(result);
            redisConnection.off(`${response}:${messageId}`);
            redisConnection.off(`${response}-failed:${messageId}`);

            clearTimeout(killswitchTimeoutId);
        });

        redisConnection.on(`${response}-failed:${messageId}`, (error, channel) => {
            res
                .status(500)
                .json(error);

            redisConnection.off(`${response}:${messageId}`);
            redisConnection.off(`${response}-failed:${messageId}`);

            clearTimeout(killswitchTimeoutId);
        });

        killswitchTimeoutId = setTimeout(() => {
            redisConnection.off(`${response}:${messageId}`);
            redisConnection.off(`${response}-failed:${messageId}`);
            res
                .status(500)
                .json({error: "Timeout error"});
        }, 5000);

        redisConnection.emit(`${action}:${messageId}`, {
            requestId: messageId,
            id: req.params.id,
            data: req.body,
            user: req.user,
            token: req.token
        });

    };
}

module.exports = {
    createRoute,
    authToken
};
