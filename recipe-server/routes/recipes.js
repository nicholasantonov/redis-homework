const express = require('express');
const router = express.Router();
const recipeData = require("../../recipe-data");
const uuid = require("node-uuid");
const utils = require('./utils');
const createRoute = utils.createRoute;
const redis = require('promise-redis')().createClient();
const cache = require('express-redis-cache')({prefix: 'cache'});
const jwt = require('jsonwebtoken');

router.get("/", cache.route('recipes', 60 * 60), (req, res) => {
    recipeData
        .getAllRecipes()
        .then((recipeList) => {
            res.json(recipeList);
        })
        .catch(() => {
            // Something went wrong with the server!
            res.sendStatus(500);
        });
});

router.get('/:id', (req, res, next) => {
    // Lol why would I abstract when I can copy paste for homework
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    jwt.verify(token, 'superSecret', function(err, decoded) {
        if (!err) {
            recipeData.getRecipe(req.params.id)
                .then(recipe => {
                    redis.lpush(`recent:${decoded.id}`, JSON.stringify(recipe)).then(res => redis.ltrim('recent', 0, 9));
                    res.json(recipe);
                })
                .catch(error => {
                    console.log(error);
                    res.json({success: false, error});
                });
        } else {
            // this route is defined weird in the assigment. Cached, but we have to do lookups for the "recently viewed" list
            next();
        }
    });
}, cache.route(60 * 60), (req, res) => {
    // we have our cake then eat it too
    recipeData.getRecipe(req.params.id)
        .then(recipe => {
            res.json(recipe);
        })
        .catch(error => {
            console.log(error);
            res.json({success: false, error});
        });
});

router.post('/', utils.authToken, createRoute('create-recipe',  'recipe-created'));
router.put('/:id', utils.authToken, createRoute('update-recipe', 'recipe-updated'));
router.delete('/:id', utils.authToken, createRoute('delete-recipe', 'recipe-deleted'));

module.exports = router;
