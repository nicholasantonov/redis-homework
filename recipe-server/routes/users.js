const express = require('express');
const router = express.Router();
const uuid = require("node-uuid");
const jwt = require('jsonwebtoken');
const recipeData = require("../../recipe-data");
const _ = require('lodash');
const redis = require('promise-redis')().createClient();
const cache = require('express-redis-cache')({prefix: 'cache'});

const utils = require('./utils');
const createRoute = utils.createRoute;

function userToPublic (user) {
    return {
        info: user.info,
        username: user.username,
        id: user._id
    };
}

router.post('/', createRoute('create-user', 'user-created'));
router.post('/session', (req, res) => {
    recipeData.getUserByName(req.body.username)
        .then(user => {
            if (!user) {
                res.json({ success: false, message: 'Authentication failed. User not found.' });
            } else {
                if (user.password != req.body.password) {
                    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                } else {

                    var token = jwt.sign(userToPublic(user), 'superSecret', {
                        expiresIn: '1h'
                    });

                    redis.hset('token', token, 'true');

                    res.json({
                        success: true,
                        message: 'Tokens are less tasty than cookies.',
                        token: token
                    });
                }
            }
        });
});
router.get('/:id', cache.route(300), (req, res) => {
    recipeData.getUserById(req.params.id)
        .then(user => res.json(userToPublic(user)));
});
router.get('/', cache.route('users', 600), (req, res) =>
    recipeData.getAllUsers()
        .then(users => users.map(userToPublic))
        .then(users => res.json(users))
        .then(_ => console.log(req.originalUrl))
        .catch(error => res.status(500).json({error}))
);
router.put('/', createRoute('modify-user', 'user-modified'));
router.delete('/', utils.authToken, createRoute('delete-user', 'user-deleted'));

module.exports = router;
