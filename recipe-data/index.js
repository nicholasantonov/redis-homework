const collections = require('./collections');
const recipeCollection = collections.recipes;
const userCollection = collections.users;
const ObjectId = require('mongodb').ObjectID;
const _ = require('lodash');


let exportedMethods = {
    getAllUsers() {
        return userCollection().then(users => users.find().toArray());
    },
    getUserFromToken(token) {
        return userCollection()
            .then(users => users.findOne({token}));
    },
    getUserById(id) {
        return userCollection()
            .then(users => users.findOne({'_id': new ObjectId(id)}));
    },
    getUserByName(username) {
        return userCollection()
            .then(users => users.findOne({username}));
    },
    modifyUser(username, newInfo) {
        return userCollection()
            .then(users => users.updateOne({username}, {$set: newInfo}));
    },
    deleteUser(username) {
        return userCollection()
            .then(users => users.deleteOne({username}));
    },
    makeUser(user) {
        return userCollection()
            .then(users => users.insertOne(_.pick(user, ['username', 'password', 'info'])))
            .then(result => userCollection())
            .then(users => users.findOne({username: user.username}));
    },
    getAllRecipes() {
        return recipeCollection().then((recipes) => {
            return recipes
                .find()
                .toArray();
        });
    },
    getRecipe(id) {
        return recipeCollection().then((recipes) => {
            return recipes.findOne({_id: new ObjectId(id)});
        });
    },
    updateRecipe(id, modified) {
        return recipeCollection()
            .then(recipes => recipes.updateOne({'_id': new ObjectId(id)}, {$set: _.omit(modified, ['_id', 'creator'])}))
            .then(res => {
                return recipeCollection().then((recipes) => {
                    return recipes.findOne({_id: new ObjectId(id)});
                });
            });
    },
    deleteRecipe(id) {
        return recipeCollection()
            .then(recipes => recipes.deleteOne({'_id': new ObjectId(id)}));
    },
    addRecipe(recipe, owner) {
        return recipeCollection().then((recipes) => {
            let newRecipe = JSON.parse(JSON.stringify(recipe));

            if (!Array.isArray(newRecipe.ingredients)) {
                throw new Error("no ingredients given");
            }

            newRecipe.ingredients.forEach(ingredient => {
                ingredient.systemTitle = ingredient.displayTitle.toString();
            });

            newRecipe.relatedIngredients = [];
            newRecipe.imageUrls = [];
            newRecipe.owner = owner;

            return recipes.insertOne(newRecipe);
        }).then((recipe) => {
            return this.getRecipe(recipe.insertedId);
        });
    },
    createRecipeRelationship(firstRecipe, firstMatchAmount, secondRecipe, secondMatchAmount) {
        return recipeCollection().then((recipes) => {
            return recipes.updateOne({
                _id: firstRecipe
            }, {
                $addToSet: {
                    relatedRecipes: {
                        _id: secondRecipe,
                        amount: firstMatchAmount
                    }
                }
            }).then(() => {
                recipes.updateOne({
                    _id: secondRecipe
                }, {
                    $addToSet: {
                        relatedRecipes: {
                            _id: firstRecipe,
                            amount: secondMatchAmount
                        }
                    }
                })
            }).then(() => {
                return recipes.find({
                    _id: [firstRecipe, secondRecipe]
                })
            });
        });
    },
    findRecipesWithIngredient(systemTitle) {
        return recipeCollection().then((recipes) => {
            return recipes
                .find({"ingredients.systemTitle": systemTitle})
                .toArray()
        });
    },
    findRecipesWithIngredients(systemTitles) {
        return recipeCollection().then((recipes) => {
            return recipes
                .find({
                "ingredients.systemTitle": {
                    $in: systemTitles
                }
            })
                .toArray()
        });
    },
    addImagesToRecipe(recipeId, imageUrlArray) {
        return recipeCollection().then((recipes) => {
            return recipes.updateOne({
                _id: recipeId
            }, {
                $addToSet: {
                    imageUrls: {
                        $each: imageUrlArray
                    }
                }
            }).then(() => {
                return this.getRecipe(recipeId);
            });
        });
    }
}

module.exports = exportedMethods;
