const recipeData = require("../recipe-data");
const fetch = require('node-fetch');
const sha = require('js-sha256').sha256;
const _ = require('lodash');
const redis = require('promise-redis')().createClient();

const NRP = require('node-redis-pubsub');
const config = {
    port: 6379, // Port of your locally running Redis server
    scope: 'recipes' // Use a scope to prevent two NRPs from sharing messages
};

const redisConnection = new NRP(config); // This is the NRP client


function userToPublic (user) {
    return {
        info: user.info,
        username: user.username,
        id: user._id
    };
}

// Note, this is really bad.
const pixabayApiKey = "3432196-710f5c9e1d0f75f6c0aa4a34a";
const basePixabayUrl = `https://pixabay.com/api/?key=${pixabayApiKey}&safesearch=true&q=`;

redisConnection.on('create-recipe:*', (data, channel) => {
    let messageId = data.requestId;

    let fullyComposeRecipe = recipeData
        .addRecipe(data.data, data.user)
        .then((newRecipe) => {
            return fetch(`${basePixabayUrl}${newRecipe.title}`).then((res) => {
                return res.json();
            }).then((response) => {
                return response
                    .hits
                    .map(x => x.previewURL)
                    .slice(0, 5);
            }).then((hits) => {
                return recipeData
                    .addImagesToRecipe(newRecipe._id, hits)
                    .then((recipeWithUrls) => {
                        return recipeData
                            .findRecipesWithIngredients(recipeWithUrls.ingredients.map(x => x.systemTitle))
                            .then(recipeList => {

                                let recipeListExceptCurrent = recipeList.filter(x => x._id !== newRecipe._id);

                                // Perform logic here Go through entire recipe list Calculate the percentage
                                // matched for each. Compose an array of data calls to setup the percentage
                                // matched Add all, then resolve to recipeWithUrls
                                return recipeWithUrls;
                            });
                    });
            }).then((recipeWithUrls) => {
                redisConnection.emit(`recipe-created:${messageId}`, recipeWithUrls);
                redis.get('cache:recipes').then(result => {
                    const arr = JSON.parse(result);
                    arr.push(recipeWithUrls);
                    redis.set('cache:recipes', JSON.stringify(arr));
                }).catch(err => console.log('error pushing'));
                redis.expire('cache:recipes', 60 * 60).catch(err => console.log('error expiring'));
            }).catch(error => {
                console.log(error);
                // we will submit errors back to the frontend
            });
        })
        .catch(error => redisConnection.emit(`recipe-created-failed:${messageId}`, {success: false, error}));
});

redisConnection.on('create-user:*', data => {
    const messageId = data.requestId;

    return recipeData.makeUser(data.data)
        .then(user => {
            const redisKey = 'cache:users';
            redis.hget(redisKey, 'body')
                .then(users => {
                    const arr = Array.isArray(JSON.parse(users)) ? JSON.parse(users) : [];
                    arr.push(user);
                    return redis.hset(redisKey, 'body', JSON.stringify(arr));
                });

            return user;
        })
        .then(user =>redisConnection.emit(`user-created:${messageId}`, user))
        .catch(err => redisConnection.emit(`user-created-failed:${messageId}, err`));
});

redisConnection.on('modify-user:*', message => {
    const messageId = message.requestId;
    redis.expire(`cache:/users/${message.user.id}`, 300);
    recipeData.modifyUser(req.user.username, _.pick(req.body, ['password', 'info']))
        .then(result => redisConnection.emit(`user-modified:${messageId}`, {success: true}))
        .catch(error => redisConnection.emit(`user-modified-failed:${messageId}`, {success: false, error}));
});

redisConnection.on('delete-user:*', message => {
    const messageId = message.requestId;
    redis.hset('token', message.token, 'false');
    redis.expire(`cache:/users/${message.user.id}`, 0);
    recipeData.deleteUser(message.user.username)
        .then(result => redisConnection.emit(`user-deleted:${messageId}`, {success: true}))
        .catch(error => redisConnection.emit(`user-deleted-failed:${messageId}`, {success: false, error}));
});


redisConnection.on('delete-recipe:*', message => {
    const messageId = message.requestId;

    recipeData.getRecipe(message.id)
        .then(recipeToUpdate => {
            if (!recipeToUpdate) {
                console.log('didnt find it');
                redisConnection.emit(`recipe-deleted-failed:${messageId}`, {success: false, error: "no recipe matches"});
                return;
            }
            if (recipeToUpdate.owner.id !== message.user.id) {
                console.log('wrong owner');
                redisConnection.emit(`recipe-deleted-failed:${messageId}`, {success: false, error: "incorrect owner"});
            } else {
                redis.expire(`cache:/recipes/${message.id}`, 0);
                recipeData.deleteRecipe(message.id)
                    .then(result => redisConnection.emit(`recipe-deleted:${messageId}`, {success: true}))
                    .catch(error => redisConnection.emit(`recipe-deleted-failed:${messageId}`, {success: false, error}));
            }
        });
});

redisConnection.on('update-recipe:*', message => {
    const messageId = message.requestId;

    recipeData.getRecipe(message.id)
        .then(recipeToUpdate => {
            if (!recipeToUpdate) {
                console.log('didnt find it');
                redisConnection.emit(`recipe-updated-failed:${messageId}`, {success: false, error: "no recipe matches"});
                return;
            }
            if (recipeToUpdate.owner.id !== message.user.id) {
                console.log('wrong owner');
                redisConnection.emit(`recipe-updated-failed:${messageId}`, {success: false, error: "incorrect owner"});
            } else {
                recipeData.updateRecipe(message.id, message.data)
                    .then(result => {
                        redis.set(`cache:/recipes/${message.id}`, JSON.stringify(result));
                        redis.expire(`cache:/recipes/${message.id}`, 60 * 60);
                        redisConnection.emit(`recipe-updated:${messageId}`, {success: true});
                    })
                    .catch(error => redisConnection.emit(`recipe-updated-failed:${messageId}`, {success: false, error}));
            }
        });
});
